# Multi-contact Path Planning for Door Opening

This repository contains a Docker container which includes:
- Robotic Vision Library (RVL) and
- ROS with packages for controlling the UR5 robot

## Installation

### Docker installation

You will need a PC that supports **Nvidia drivers and CUDA** (don't need to install them on your own) in order to launch TensorMask.

Ubuntu: 
Install Docker from the [official page](https://docs.docker.com/engine/install/ubuntu/). Don't forget to do the [postinstall steps](https://docs.docker.com/engine/install/linux-postinstall/).

Windows:
Install Docker from the [official page](https://docs.docker.com/desktop/install/windows-install/). To run the necessary shell scripts, you can use [WSL](https://learn.microsoft.com/en-us/windows/wsl/install).


### Container setup (Linux)

Enter the repository:
```bash
cd path_planning_docker
```

Run the shell script to build the Docker image:
```bash
./build_docker.sh
```

To be able to show any windows from the container, run:
```bash
xhost +
```
This command should be ran on every login. To avoid it, you can add it to .bashrc:
```bash
echo 'xhost +' >> ~/.bashrc 
```

After installation, run the Docker container:
```bash
./run_docker.sh
```
The scripts will first stop and remove the current container if it exists. If it doesn't it will output an error that it can't stop or remove the container, but they can be ignored.

The container is now initialized and running. You can run `ls` command to check if the rvl-linux and ferit_ur5_ws directories are listed. If not, exit the current instance of the container and change the first part of the first --volume argument to look like this:
```bash
--volume="/path/to/your/project/dir:/home/RVLuser" \
```
Rerun the container with:
```bash
./run_docker.sh
```

If you need more terminals inside the container, you can create a new one with:
```bash
docker exec -it path_planning bash
```

### VSCode setup

While the container is running, you can setup the VSCode to work inside the container.
A tutorial on the developing inside a container can be found [here](https://code.visualstudio.com/docs/devcontainers/containers).

Inside the container, you may consider installing some extensions: Python, C/C++, ROS, CMake, Makefile Tools. These aren't necessary, but are helpful. 

You don't need to setup any VSCode environments as they are already set to work with the container's paths (for RVL and ferit_ur5_ws). 


## Usage

The current 'user' RVLuser is already root.

### RVL usage

Inside the container, navigate to RVL's root directory:
```bash
cd /home/RVLuser/rvl-linux
```
Build the library with:
```bash
make
```
Configure the necessary .cfg files in the root directory of the library and run the program. For example:
```bash
./build/bin/RVLMotionDemo
```

#### Before starting the multi-contact path planning algorithm:
Start the `push_demo.py` located in rvl-linux/python/DDMan script to generate feasible poses. You can change the configuration in `push.py`. 

### ROS usage
Inside the container, in one terminal, initialize roscore:
```bash
roscore
```
In another terminal, navigate to the ferit_ur5_ws directory:
```bash
cd /home/RVLuser/ferit_ur5_ws
```
build the packages:
```bash
catkin build
```
source the project:
```bash
source devel/setup.bash
```


### Path planning & Gazebo
# In Gazebo
Enter the ferit_ur5_ws directory (make if needed) and source:
```bash
cd /home/RVLuser/ferit_ur5_ws
catkin build
source devel/setup.bash
```
Run the Gazebo simulation with (without path planning):
```bash
roslaunch ur5_robotiq_ft_3f_moveit_config demo_gazebo.launch
```


You can start the multi-contact path planning algorithm (the experiments) - this does not need demo_gazebo launched.
```bash
roslaunch path_planning start_multi_contact_simulation_experiment.launch
```

# In the real world
In separate terminals, run these:
```bash
roslaunch ur_robot_driver ur5_bringup.launch robot_ip:=192.168.10.14 kinematics_config:="/home/RVLuser/ferit_ur5_ws/ur5_calibration.yaml"
```
```bash
roslaunch ur5_robotiq_ft_3f_moveit_config moveit_planning_execution.launch
```
Run the experiment:
```bash
rosrun path_planning node_start_real_experiment.py
```


<!-- ## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change. -->


## License

[MIT](https://choosealicense.com/licenses/mit/)